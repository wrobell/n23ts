-r requirements.txt

mypy
pytest-cov>=2.6.0
pytest-asyncio
pytest-timeout
uvloop>=0.13.0
