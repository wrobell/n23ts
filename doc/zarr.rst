Zarr Format
===========

n23ts file is stored in Zarr format.

The following rules and conventions apply

- if file is closed properly, then all data can be accessed with Zarr
  library
- each time series dataset is chunked
- chunking is allowed only across axis 0, this is the length of a dataset
- chunk compression is done with Zstandard compressor by default
- each group has one timestamp dataset and missing values of other datasets
  are represented by dataset fill value

.. vim: sw=4:et:ai
