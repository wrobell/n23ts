Installation
============

1. Install `libaio` library.
2. Install `cython`.
3. Install `n23ts`.

.. vim: sw=4:et:ai
