.PHONY=check test

check:
	mypy n23ts

test:
	pytest -vv
