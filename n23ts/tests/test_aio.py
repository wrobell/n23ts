#
# n23ts - time series database
#
# Copyright (C) 2019 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import asyncio
import fcntl
import os.path
from contextlib import ExitStack

import pytest

import n23ts.aio as io

pad = lambda v: bytes.ljust(v, 4096, b'\x00')

@pytest.fixture
def two_files(tmpdir):
    names = [os.path.join(tmpdir, fn) for fn in ('t1.bin', 't2.bin')]
    with ExitStack() as stack:
        files = [[fn, stack.enter_context(io.fopen(fn, 8192))] for fn in names]
        yield files

def test_fopen(tmpfile):
    """
    Test opening file in write mode.
    """
    with io.fopen(tmpfile, 4096) as f:
        mode = fcntl.fcntl(f, fcntl.F_GETFL)
        assert mode & os.O_RDWR == os.O_RDWR
        os.write(f, b'x' * 4)

    with open(tmpfile, 'rb') as f:
        assert pad(b'xxxx') == f.read()

@pytest.mark.timeout(3)
@pytest.mark.asyncio
async def test_write_bytes(tmpfile):
    """
    Test asynchronous write of bytes.
    """
    with io.context() as ctx, io.fopen(tmpfile, 4096) as f:
        # write few bytes, but expect block to be written (4096 bytes);
        # include some zero bytes, this is \x00 in the middle to see if the
        # data is not truncated
        await ctx.write([(f, b'a\x00bc\x00d', 0)])

    with open(tmpfile, 'rb') as f:
        assert pad(b'a\x00bc\x00d') == f.read()

@pytest.mark.timeout(3)
@pytest.mark.asyncio
async def test_write_bytearray(tmpfile):
    """
    Test asynchronous write of bytearray.
    """
    with io.context() as ctx, io.fopen(tmpfile, 4096) as f:
        # as in `test_write_bytes` test, prepare dataset so it is small and
        # contains null bytes in the midddle
        await ctx.write([(f, bytearray(b'a\x00bc\x00d'), 0)])

    with open(tmpfile, 'rb') as f:
        assert pad(b'a\x00bc\x00d') == f.read()

@pytest.mark.timeout(3)
@pytest.mark.asyncio
async def test_write_multiple(two_files):
    """
    Test asynchronous parallel write to multiple files.
    """
    names, fds = zip(*two_files)
    with io.context() as ctx:
        data = [b'abc', b'xyz_']
        offset = [0, 0]
        items = list(zip(fds, data, offset))
        await ctx.write(items)

        data = [b'def', b'0123']
        offset = [4096, 4096]
        items = list(zip(fds, data, offset))
        await ctx.write(items)

    with open(names[0], 'rb') as f:
        result = f.read()
        assert pad(b'abc') + pad(b'def') == result

    with open(names[1], 'rb') as f:
        result = f.read()
        assert pad(b'xyz_') + pad(b'0123') == result

@pytest.mark.timeout(3)
@pytest.mark.asyncio
async def test_write_in_progress(two_files):
    """
    Test asynchronous write when write is in progress.
    """
    names, fds = zip(*two_files)
    with io.context() as ctx:
        data = [b'abc', b'xyz_']
        offset = [0, 0]
        items = list(zip(fds, data, offset))
        with pytest.raises(io.AIOError) as ex_ctx:
            await asyncio.gather(ctx.write(items), ctx.write(items))

@pytest.mark.timeout(3)
@pytest.mark.asyncio
async def test_write_zero_data(tmpfile):
    """
    Test asynchronous write when no data to write.
    """
    with io.context() as ctx:
        result = await ctx.write([])
        assert result is None

@pytest.mark.timeout(3)
@pytest.mark.asyncio
async def test_write_data_too_long(tmpfile):
    """
    Test asynchronous write error when data to write is too long.
    """
    with io.context() as ctx, io.fopen(tmpfile, 4096) as f:
        with pytest.raises(io.AIOError) as ex_ctx:
            await ctx.write([[f, b'x' * 4097, 0]])

# vim: sw=4:et:ai
