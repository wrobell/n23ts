#
# n23ts - time series database
#
# Copyright (C) 2019 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Unit tests for core `n23ts` classes and functions.
"""

import numpy as np
import os.path
import zarr

import pytest

def test_create_file(n23ts_file):
    """
    Test creating file.
    """
    assert isinstance(n23ts_file._file, zarr.Group)

def test_create_dataset_uint32(n23ts_file):
    """
    Test creating dataset for uint32 type.
    """
    n23ts_file.create('test', np.uint32, fill_value=0)
    ds = n23ts_file['test']

    assert np.uint32 == ds.dtype
    assert (0,) == ds.shape
    assert (1024,) == ds.chunks
    assert 0 == ds.fill_value

    # after resize, the array is filled with fill value
    ds.resize((10,))
    assert [0] * 10 == list(ds)

def test_create_dataset_float_array(n23ts_file):
    """
    Test creating dataset for float array type.
    """
    n23ts_file.create('test', np.float32, ushape=(3,), fill_value=float('nan'))
    ds = n23ts_file['test']

    assert np.float32 == ds.dtype
    assert (0, 3) == ds.shape
    assert (1024, 3) == ds.chunks
    assert np.isnan(ds.fill_value)

    # after resize, the array is filled with fill value
    ds.resize((10, 3))
    assert np.all(np.isnan(ds))

def test_create_dataset_string(n23ts_file):
    """
    Test creating dataset for string type.
    """
    n23ts_file.create('test', str)
    ds = n23ts_file['test']

    assert np.dtype('O') == ds.dtype
    assert (0,) == ds.shape
    assert (1024,) == ds.chunks
    assert None == ds.fill_value

    # after resize, the array is filled with fill value
    ds.resize((10,))
    assert [None] * 10 == list(ds)

def test_create_dataset_bytes(n23ts_file):
    """
    Test creating dataset for bytes type.
    """
    n23ts_file.create('test', bytes)
    ds = n23ts_file['test']

    assert np.dtype('O') == ds.dtype
    assert (0,) == ds.shape
    assert (1024,) == ds.chunks
    assert None == ds.fill_value

    # after resize, the array is filled with fill value
    ds.resize((10,))
    assert [None] * 10 == list(ds)

def test_append(n23ts_file):
    """
    Test appending data to n23ts database.
    """
    n23ts_file.create('test', np.float32)
    n23ts_file.append(1024.0, 'test', 1.0)
    assert 1 == len(n23ts_file._queue)

# vim: sw=4:et:ai
