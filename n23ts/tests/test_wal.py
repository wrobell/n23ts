#
# n23ts - time series database
#
# Copyright (C) 2019 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Unit tests for reading and writing WAL files.
"""

import array
import numpy as np
import os.path
import msgpack

from n23ts import wal

import pytest

def test_identity():
    """
    Test identity function.
    """
    result = wal.identity(1, 3, 5, x=1, y=2)
    assert 1 == result

def test_unit_size_scalar():
    """
    Test calculation of unit size for scalar value.
    """
    v = np.zeros(12, dtype=np.uint32)
    result = wal.unit_size(v)
    assert 4 == result 

def test_unit_size_array():
    """
    Test calculation of unit size for array value.
    """
    v = np.zeros(12, dtype=np.uint32).reshape(4, 3)
    result = wal.unit_size(v)
    assert 12 == result 

def test_unit_size_str():
    """
    Test calculation of unit size for string value.
    """
    v = np.array(['a', 'b', 'c'], dtype=str)
    result = wal.unit_size(v)
    assert 4 == result 

def test_data_fixed_scalar():
    """
    Test conversion of binary data into scalar value.
    """
    dt = np.uint16
    v = b''.join(dt(i).tobytes() for i in range(6))
    result = wal.wal_data_fixed(v, np.uint16, ())
    assert list(range(6)) == result

def test_data_fixed_array():
    """
    Test conversion of binary data into fixed length array of scalar values.
    """
    dt = np.uint16
    v = b''.join(dt(i).tobytes() for i in range(6))
    result = wal.wal_data_fixed(v, dt, (3,))
    assert [[0, 1, 2], [3, 4, 5]] == result

def test_read_wal_fixed(tmpdir):
    """
    Test reading data from WAL file for fixed length data.
    """
    data = np.array(range(6), dtype=np.uint32)
    fn = os.path.join(tmpdir, 'data')
    with open(fn, 'wb') as f:
        f.write(data.tobytes())

    with open(fn, 'rb') as f:
        result = wal.read_wal_fixed(f, 4)
    expected = array.array('I', range(6)).tobytes()
    assert expected == result

def test_read_wal_fixed_incomplete(tmpdir):
    """
    Test reading data from WAL file for fixed length data when
    the file is truncated.
    """
    data = np.array(range(6), dtype=np.uint32)
    fn = os.path.join(tmpdir, 'data')
    with open(fn, 'wb') as f:
        buff = data.tobytes()
        f.write(buff)
        f.truncate(len(buff) - 2)

    with open(fn, 'rb') as f:
        with pytest.raises(ValueError) as ctx_ex:
            wal.read_wal_fixed(f, 4)

def test_read_wal_var(tmpdir):
    """
    Test reading data from WAL file for variable length data.
    """
    data = ['abc', 'xyz', 'nm']
    buff = b''.join(msgpack.dumps(v) for v in data)
    fn = os.path.join(tmpdir, 'data')
    with open(fn, 'wb') as f:
        f.write(buff)

    with open(fn, 'rb') as f:
        result = wal.read_wal_var(f, 0)
    assert ['abc', 'xyz', 'nm'] == result

def test_merge_data():
    """
    Test merging WAL data.
    """
    data = [
        b'1234',
        b'56',

        b'1234',
        b'56',
        b'78',

        b'12',

        b'1234567',
    ]
    result = wal.merge_data(8, data)
    expected = [b'123456', b'12345678', b'12', b'1234567']
    assert expected == list(result)

# vim: sw=4:et:ai
