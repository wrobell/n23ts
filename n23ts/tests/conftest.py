#
# n23ts - time series database
#
# Copyright (C) 2019 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os.path

import pytest

import n23ts

@pytest.fixture
def tmpfile(tmpdir):
    """
    Path to a temporary file.
    """
    return os.path.join(tmpdir, 'test.bin')

@pytest.fixture
def n23ts_file(tmpdir):
    """
    Create temporary n23ts File.
    """
    f = n23ts.File(os.path.join(tmpdir, 'fn.zarr'))
    return f

# vim: sw=4:et:ai
