#
# n23ts - time series database
#
# Copyright (C) 2019 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# cython: language_level=3

from libc.stdlib cimport malloc, free
from libc.stdint cimport uint64_t
from libc.string cimport memcpy, memset
from posix.stdlib cimport posix_memalign
from posix.unistd cimport close, read

import asyncio
import os
import time
import typing as tp
from contextlib import contextmanager

# write data item passed to IOContext.write method
WriteData = tp.Tuple[int, tp.Union[bytes, bytearray], int]

cdef extern from 'sys/time.h':
    ctypedef struct timespec:
        pass

cdef extern from '<sys/eventfd.h>':
    enum: EFD_NONBLOCK
    int eventfd(unsigned int, int)
    int eventfd_read(int, uint64_t*);


cdef extern from 'libaio.h':
    ctypedef struct io_context_t:
        pass

    cdef struct io_event:
        pass

    cdef struct iocb:
        pass

    int io_setup(int, io_context_t*)
    int io_submit(io_context_t, long, iocb *ios[])
    int io_getevents(io_context_t, long, long, io_event*, timespec*)
    int io_destroy(io_context_t)

    void io_prep_pwrite(iocb*, int fd, const void*, int, long long)
    void io_set_eventfd(iocb*, int)

DEFAULT_PAGE_SIZE = 4096

cdef class IOContext:
    # maximum number of async i/o events
    cdef unsigned int max_events

    # block size of physical device or multiple of it; defaults to 4KB as
    # it seems a lot of block devices are optimized for this size
    cdef unsigned int block_size

    # filesystem page size

    cdef io_context_t obj

    cdef object loop
    cdef object task

    cdef iocb **control_blocks
    cdef io_event *events
    cdef void **buffers

    cdef int event_fd

    # if n_scheduled > 0, then write is in progress
    cdef int n_scheduled

    # start and end time of idle phase; the variables define period of time
    # when the asyncio loop is not blocked
    cdef double _idle_start
    cdef double _idle_end

    def __cinit__(self, unsigned int max_events, unsigned int block_size):
        self.max_events = max_events
        self.block_size = block_size

        self.control_blocks = <iocb**>malloc(max_events * sizeof(iocb*))
        for i in range(self.max_events):
            self.control_blocks[i] = <iocb*>malloc(sizeof(iocb))

        self.events = <io_event*>malloc(max_events * sizeof(io_event))

        self.buffers = <void**>malloc(max_events * sizeof(void*))
        for i in range(self.max_events):
            posix_memalign(&self.buffers[i], self.block_size, self.block_size)

        io_setup(max_events, &self.obj)

        self.event_fd = eventfd(0, EFD_NONBLOCK)
        self.loop = asyncio.get_event_loop()
        self.loop.add_reader(self.event_fd, self._handle_event)

        self.task = None
        self.n_scheduled = 0

        self._idle_start = 0
        self._idle_end = 0

    cdef destroy(self):
        cdef iocb *cb
        cdef void *buff

        io_destroy(self.obj)
        self.loop.remove_reader(self.event_fd)
        close(self.event_fd)

        for i in range(self.max_events):
            cb = self.control_blocks[i]
            free(cb)
        free(self.control_blocks)

        free(self.events)

        for i in range(self.max_events):
            buff = self.buffers[i]
            free(buff)
        free(self.buffers)

    async def write(self, items: tp.List[WriteData]):
        cdef int fd, off, r, i
        cdef unsigned int n
        cdef const unsigned char[:] data

        if self.n_scheduled > 0:
            raise AIOError('There are pending write operations')

        self.n_scheduled = len(items)
        if self.n_scheduled == 0:
            return

        for i in range(self.n_scheduled):
            fd, data, off = items[i]
            n = len(data)
            if n > self.block_size:
                raise AIOError('Data of request {} exceeds block size')

            # reset buffer first, then copy data
            memset(self.buffers[i], 0, self.block_size)
            memcpy(self.buffers[i], <char*>&data[0], n)

            io_prep_pwrite(
                self.control_blocks[i],
                fd,
                self.buffers[i],
                # the write has to be of block size, otherwise io_submit
                # blocks (tested with ext4)
                self.block_size,
                off
            )
            io_set_eventfd(self.control_blocks[i], self.event_fd)

        r = io_submit(self.obj, self.n_scheduled, self.control_blocks)
        if r != self.n_scheduled:
            raise AIOError('Error scheduling I/O write: {}'.format(r))

        self.task = self.loop.create_future()

        self._idle_start = time.monotonic()
        try:
            return (await self.task)
        finally:
            self.task = None
            self.n_scheduled = 0

    def _handle_event(self):
        cdef int r
        cdef uint64_t i, n_events

        self._idle_end = time.monotonic()

        r = eventfd_read(self.event_fd, &n_events)
        if r < 0:
            self._set_error('Failed to get result of I/O operation')
            return

        # according to eventfd read, we are guaranteed to get n_events i/o
        # events; the max can be n_scheduled, but if r > n_events, then we
        # have to perform eventfd read again; let it to be done by another
        # `_handle_event` call
        r = io_getevents(
            self.obj,
            n_events,
            n_events,
            self.events,
            NULL
        )
        if r < 0:
            self._set_error(
                'Failed to get result of I/O operation: {}'.format(r)
            )
            return

        self.n_scheduled -= n_events
        if self.n_scheduled <= 0:
            self.task.set_result(None)

    cdef _set_error(self, msg):
        """
        Set error for current asynchronous task.
        """
        assert self.task is not None
        self.task.set_exception(AIOError(msg))

    def _get_idle(self):
        return self._idle_end - self._idle_start

class AIOError(Exception): pass

@contextmanager
def context(max_events: int=16, block_size: int=4096):
    ctx = IOContext(max_events, block_size)
    try:
        yield ctx
    finally:
        ctx.destroy()

@contextmanager
def fopen(fn: str, size: int, direct=True, datasync=True) -> int:
    """
    Open file for writing.

    The file is opened in binary mode with predefined size.

    The `direct` and `datasync` parameters are taken into account only when
    file is opened for writing.

    :param fn: Name of file to open.
    :param size: Size of the file to be opened.
    :param direct: Open with `O_DIRECT` flag if true.
    :param datasync: Open with `O_DSYNC` flag if true.
    """
    flags = os.O_CREAT | os.O_RDWR
    if direct: flags |= os.O_DIRECT
    if datasync: flags |= os.O_DSYNC

    fd = os.open(fn, flags)
    try:
        os.posix_fallocate(fd, 0, size)
        yield fd
    finally:
        os.close(fd)

# vim: sw=4:et:ai
