#
# n23ts - time series database
#
# Copyright (C) 2019 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import numpy as np
import typing as tp
import zarr
from collections import deque

from .wal import Wal

Shape = tp.Tuple[int, ...]
Timestamp = tp.Union[float,np.datetime64]

class File:
    """
    File for n23ts database.
    """
    def __init__(self, fn: str, mode='a'):
        """
        Create and initialize n23ts file.

        The file is compatible with Zarr format.
        """
        self._file = zarr.open_group(store=fn, mode=mode)
        self._queue = deque([], 10000)
        self._wal = Wal(fn)

    def create(
            self,
            name: str,
            dtype: tp.Any,
            chunks: int=1024,
            ushape: Shape=(),
            fill_value: tp.Any=None
        ):
        """
        Create new dataset.

        If dataset exists, then do nothing.

        :param name: Name of dataset.
        :param dtype: Type of dataset scalar value.
        :param chunks: Chunk size.
        :param ushape: Unit shape - shape of dataset item.
        :param fill_value: Fill value for uninitialized parts of dataset.
        """
        if name not in self._file:
            self._file.create(
                name,
                dtype=dtype,
                chunks=(chunks,) + ushape,
                shape=(0,) + ushape,
                fill_value=fill_value
            )

    def __getitem__(self, name: str):
        return self._file[name]

    def append(self, ts: Timestamp, name: str, value: tp.Any):
        """
        Add time series value to database.

        The data enqueue and is not saved to the underlying storage until
        the `write` method is called.

        :param ts: Timestamp value.
        :param name: Name of time series dataset.
        :param value: Time series value.
        """
        self._queue.append((ts, name, value))

    async def write(self):
        """
        Write appended data to database into underlying storage.
        """
        await self._wal.write(self._queue)

    def close(self):
        pass

# vim: sw=4:et:ai
