#
# n23ts - time series database
#
# Copyright (C) 2019 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

"""
WAL file writing and reading.
"""

import cytoolz.itertoolz as itz
import logging
import msgpack
import numpy as np
import operator
import os.path
import typing as tp
from dataclasses import dataclass
from functools import partial

logger = logging.getLogger(__name__)

T = tp.TypeVar('T')
Shape = tp.Tuple[int, ...]

Unpacker = partial(msgpack.Unpacker, raw=False)

class WalError(Exception):
    """
    WAL file base error.
    """

class WalReadError(Exception):
    """
    WAL file read error.
    """

def read_wal_fixed(f, usize: int) -> bytes:
    """
    Read WAL data for fixed length data.
    """
    data = f.read()
    n = len(data)
    if n % usize != 0:
        raise ValueError()
    return data

def read_wal_var(f, size: int=-1) -> tp.Union[tp.List[str], tp.List[bytes]]:
    """
    Read WAL data for variable length data.
    """
    decoder = Unpacker(f)
    data = [v for v in decoder]
    return data

def wal_data_fixed(data, dtype, ushape):
    """
    Convert binary data to an array of fixed length data.
    """
    result = np.frombuffer(data, dtype=dtype)
    n = len(result)
    ulen = unit_len(ushape)
    assert n % ulen == 0
    count = n // ulen
    return result.reshape((count,) + ushape).tolist()

def move_wal_to_recovery(ds, fn):
    """
    Move WAL file to recovery directory.
    """
    base_path = array_dir_path(ds, 'recovery')

    ts = datetime.utcnow()
    fn_recovery = '{}-{}-{}.wal'.format(ds.name, ts.isoformat(), chunk)
    fn_out = os.path.join(base_path, fn_recovery)
    rename(fn, fn_out)
    return fn_out

def identity(v, *args, **kw):
    """
    Identity function for its first argument.
    """
    return v

def unit_size(ds):
    """
    Calculate size of value for given dataset.

    The size is number of bytes required to store a value on a disk.
    """
    return ds.dtype.itemsize * unit_len(ds.shape[1:])

def unit_len(ushape):
    """
    Calculate number of items for value having the specified shape.
    """
    return int(np.prod(ushape))

@dataclass
class Reader:
    read_wal: tp.Callable
    to_list: tp.Callable

WAL_READER: tp.Dict[str, Reader] = {
    'variable': Reader(read_wal_var, identity),
    'fixed': Reader(read_wal_fixed, wal_data_fixed),
}

def read_wal(fn: str, ds):
    """
    Read WAL file.

    :param fn: Filename of the WAL file.
    :param ds: Dataset associated with the WAL file.
    """
    try:
        rt_name = 'variable' if ds.dtype in (str, bytes) else 'fixed'
        assert rt_name in WAL_READER

        rt = WAL_READER[rt_name]
        size = unit_size(ds)
        with open(fn, 'r') as f:
            data = rt.read_wal(fn, size)
        return rt.to_list(data, size)
    except:
        fn_out = move_wal_to_recovery(ds, fn)
        logger.warning(
            'cannot read wal file, moved from {} to {}'.format(fn, fn_out)
        )
        return []

class Wal:
    def __init__(self, fn: str):
        self._base_dir = os.path.join(fn, '.n23ts', 'wal')

    async def write(self, data: tp.Iterable):
        pass

def group_timeseries(data: tp.Iterable):
    items = ((t, get_parent(n), n, v) for t, n, v in data)
    items = itz.groupby(operator.itemgetter(0, 1), items)

def get_parent(name: str):
    return name.rsplit('/', 1)[0]

def merge_data(n: int, data: tp.Iterable[bytes]) -> tp.Iterable[bytes]:
    """
    Merge data items, so new item is shorter than `n`.

    :param n: Maximum length of new item.
    :param data: Collection of data items.
    """
    current = bytearray()
    for v in data:
        if len(current) + len(v) > n:
            yield bytes(current)
            current = bytearray()
        current.extend(v)
    yield bytes(current)

# vim: sw=4:et:ai
