#!/usr/bin/env python3
#
# n23ts - time series database
#
# Copyright (C) 2019 by Artur Wroblewski <wrobell@riseup.net>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import asyncio
import fcntl
import glob
import hashlib
import mmap
import os
import os.path
import time
import tempfile
import uvloop
from contextlib import contextmanager, ExitStack
from functools import partial
from shutil import rmtree

import n23ts.aio as io

N_TESTS = 8
COUNT = 100
BLOCK = 4096
DATA = b'x' * BLOCK

@contextmanager
def open_files(n, dir, prefix, direct=True, datasync=True):
    names = ('{}-{:04d}.bin'.format(prefix, i) for i in range(n))
    names = (os.path.join(dir, fn) for fn in names)
    fopen = partial(io.fopen, direct=direct, datasync=datasync)
    with ExitStack() as stack:
        files = [stack.enter_context(fopen(fn, COUNT * BLOCK)) for fn in names]
        yield files

def to_ms_current(ts):
    t = time.monotonic() - ts
    return to_ms(t)

def to_ms(td):
    return td * 1e3

def test_async(n, dir):
    loop = asyncio.get_event_loop()
    with io.context(n, BLOCK) as ctx, open_files(n, dir, 'async') as files:
        for i in range(COUNT):
            items = [(f, DATA, i * BLOCK) for f in files]
            task = ctx.write(items)

            ts = time.monotonic()
            loop.run_until_complete(task)
            print('{},async,{},{}'.format(n, to_ms_current(ts), to_ms(ctx._get_idle())))

def test_serial(n, dir):
    loop = asyncio.get_event_loop()
    # use default pool executor; having more threads is in-practical
    run = partial(loop.run_in_executor, None, os.pwrite)

    with open_files(n, dir, 'serial', direct=False) as files:
        for i in range(COUNT):
            k = i * BLOCK
            tasks = [run(f, DATA, k) for f in files]

            ts = time.monotonic()
            loop.run_until_complete(asyncio.gather(*tasks))
            print('{},write,{},{}'.format(n, to_ms_current(ts), 0))

def mmap_write(m, data, k):
    m[k: k + BLOCK] = data
    m.flush(k, BLOCK)

def test_mmap(n, dir):
    loop = asyncio.get_event_loop()
    run = partial(loop.run_in_executor, None, mmap_write)

    with open_files(n, dir, 'mmap', direct=False, datasync=False) as files, \
            ExitStack() as stack:

        mmaps = [stack.enter_context(mmap.mmap(f, 0)) for f in files]
        for i in range(COUNT):
            k = i * BLOCK
            tasks = [run(m, DATA, k) for m in mmaps]
            ts = time.monotonic()
            loop.run_until_complete(asyncio.gather(*tasks))
            print('{},mmap,{},{}'.format(n, to_ms_current(ts), 0))

def check(dir):
    files = glob.glob(os.path.join(dir, '*.bin'))
    hashes = set()
    for fn in files:
        with open(fn, 'rb') as f:
            m = hashlib.md5()
            m.update(f.read())
            hashes.add(m.digest())
    assert len(set(hashes)) == 1

uvloop.install()
print('n,type,time,idle')
for i in range(N_TESTS):
    n = 2 ** i
    tmpdir = tempfile.mkdtemp(dir='.')
    try:
        test_serial(n, tmpdir)
        test_async(n, tmpdir)

        # parallel execution of mmap is very inneficient, so its testing can be
        # skipped by default
        # test_mmap(n, tmpdir)

        check(tmpdir)
    finally:
        rmtree(tmpdir)

# vim: sw=4:et:ai
